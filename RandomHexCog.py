import discord
from discord.ext import commands
import random

class RandomHexCog():
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def hex(self, ctx):
        color = '{:06x}'.format(random.randint(0, 256**3))

        embed=discord.Embed()
        embed.color = discord.Colour(int(color, 16))
        embed.description = f"**{ctx.author.name}**, here is your random Hex Color: **#{color}**"
        embed.title = "Random Hex Colour"
        embed.set_thumbnail(url = f"https://dummyimage.com/38x38/{color}&text=+")
		
        await ctx.send(embed=embed)
		
def setup(bot):
    bot.add_cog(RandomHexCog(bot))