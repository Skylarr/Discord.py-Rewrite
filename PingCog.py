import discord
from discord.ext import commands
import random
import asyncio

class PingCog():
	def __init__(self, bot):
		self.bot = bot

    @commands.command()
    async def ping(self, ctx):
    
        try: # This will try to delete the command from the chat!
            await ctx.delete()
        except: # And this will happen when it failed!
            pass
    
        pingms = "{}".format(int(self.bot.latency * 1000)) #This will caclulate the ping in milliseconds
        pings = "{}".format(int(self.bot.latency * 1)) # This will calculate it in seconds
        
        message = await ctx.send("Calculating some shit in the background... beep beep...") #Just a fun message before showing the ping!
        await asyncio.sleep(3)
        await message.edit(content = f"Pong! - My latency is **{pings}**s | **{pingms}**ms", delete_after = 15)
		
def setup(bot):
    bot.add_cog(PingCog(bot))
