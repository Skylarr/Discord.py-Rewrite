import discord
import os
import sys
from .utils         import checks # Here we import the checks.py file from utils folder.
from discord.ext    import commands

class OwnerCog():
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.is_owner() # /utils/checks.py is required for this (It checks if you're the real bot owner)
    async def shutdown(self, ctx):

        try: # This will try to delete the command from the chat!
            await ctx.delete()
        except: # And this will happen when it failed!
            pass

        await ctx.send(f"**{self.bot.user.name}** is succesfully shutted down!")
        await self.bot.logout()

    @commands.command()
    @commands.is_owner() # And again, the same!
    async def reboot(self, ctx):

        try: # This will try to delete the command from the chat!
            await ctx.delete()
        except: # And this will happen when it failed!
            pass

        await ctx.send(f"**{self.bot.user.name}** is rebooting, one moment please!")
        os.execve(sys.executable, ['python'] + sys.argv, os.environ)
		
def setup(bot):
    bot.add_cog(OwnerCog(bot))